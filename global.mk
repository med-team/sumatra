
LIBSUMAPATH = -L./sumalibs

LIBSUMA = ./sumalibs/libsuma.a

CC=gcc
LDFLAGS=


ifeq ($(CC),gcc)
        CFLAGS = -O3 -s -DOMP_SUPPORT -w
else
        CFLAGS = -O3 -w
endif


default: all

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $< $(LIB)


########
#
# libraries compilation
#
########

./sumalibs/libsuma.a:
	$(MAKE) -C ./sumalibs
