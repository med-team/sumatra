PREFIX=/usr/local

CFLAGS=-I$(PREFIX)/include

EXEC = sumatra

SUMATRA_SRC = sumatra.c \
			  mtcompare_sumatra.c
			              
SUMATRA_OBJ = $(patsubst %.c,%.o,$(SUMATRA_SRC))

SRCS = $(SUMATRA_SRC)

LIB = -lsuma -lz -lm

include ./global.mk

all: $(EXEC)


########
#
# sumatra compilation
#
########

# executable compilation and link

sumatra: $(SUMATRA_OBJ)
	$(CC) $(LDFLAGS) -o $@ -pthread $(SUMATRA_OBJ) $(LIBSUMAPATH) $(LIB)
	
########
#
# project management
#
########

clean:
	rm -f $(SUMATRA_OBJ)	
	rm -f $(EXEC)
	$(MAKE) -C ./sumalibs clean

install: all
	install -d $(DESTDIR)$(PREFIX)/bin/
	install -m 755 $(EXEC) $(DESTDIR)$(PREFIX)/bin/
